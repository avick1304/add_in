// AddInCOM.cpp : Implementation of CAddInCOM

#include "stdafx.h"
#include "AddInCOM.h"


static IAsyncEvent *pAsyncEvent = NULL;



// CAddInCOM
//---------------------------------------------------------------------------//
BOOL CAddInCOM::LoadProperties()
{
	
	if (!m_iProfile)
		return FALSE;

	HRESULT hRes;
	wchar_t* csProperty = L"Enabled:0";;
	VARIANT varEnabled;

	::VariantInit(&varEnabled);
	V_VT(&varEnabled) = VT_I4;
	hRes = m_iProfile->Read(::SysAllocString(csProperty), &varEnabled, NULL);

	if (hRes != S_OK)
		return FALSE;

	m_boolEnabled = V_I4(&varEnabled) ? FALSE : TRUE;
	return TRUE;
		
	return S_FALSE;
}
//---------------------------------------------------------------------------//
void CAddInCOM::SaveProperties()
{
	if (!m_iProfile)
		return;

	wchar_t* csProperty = L"Enabled";
	VARIANT varEnabled;

	::VariantInit(&varEnabled);
	V_VT(&varEnabled) = VT_I4;
	V_I4(&varEnabled) = m_boolEnabled ? 1 : 0;
	m_iProfile->Write(::SysAllocString(csProperty), &varEnabled);
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::Init(IDispatch *pConnection)
{
	if (!pConnection)
		return E_FAIL;
	m_iConnect = pConnection;

	UINT tiCount = 0;


	m_iConnect->QueryInterface(IID_IErrorLog, (void **)&m_iErrorLog);
	m_iConnect->QueryInterface(IID_IAsyncEvent, (void **)&m_iAsyncEvent);
	m_iConnect->QueryInterface(IID_IStatusLine, (void **)&m_iStatusLine);
	m_iConnect->GetTypeInfoCount(&tiCount);

	BSTR csProfileName = ::SysAllocString(L"TestAddInProfName");
	m_iProfile = NULL;
	pConnection->QueryInterface(IID_IPropertyProfile, (void **)&m_iProfile);

	if (m_iProfile)
	{
		m_iProfile->RegisterProfileAs(csProfileName);

		if (LoadProperties() == FALSE)
			m_boolEnabled = TRUE;
	}
	::SysFreeString(csProfileName);

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::Done()
{
	SaveProperties();

	if (m_iStatusLine)
		m_iStatusLine->Release();

	if (m_iProfile)
		m_iProfile->Release();

	if (m_iAsyncEvent)
		m_iAsyncEvent->Release();

	if (m_iErrorLog)
		m_iErrorLog->Release();
	if (m_iConnect)
		m_iConnect.Release();

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetInfo(SAFEARRAY **pInfo)
{
	// Component should put supported component technology version 
	// in VARIANT at index 0     
	long lInd = 0;
	VARIANT varVersion;
	V_VT(&varVersion) = VT_I4;
	// This component supports 2.0 version
	V_I4(&varVersion) = 2000;
	SafeArrayPutElement(*pInfo, &lInd, &varVersion);

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////
// ILanguageExtender
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::RegisterExtensionAs(BSTR *bstrExtensionName)
{
	wchar_t* csExtenderName = L"RBSoftExtension";
	*bstrExtensionName = ::SysAllocString(csExtenderName);

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetNProps(long *plProps)
{
	//// You may delete next lines and add your own implementation code here

	*plProps = ePropLast;

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::FindProp(BSTR bstrPropName, long *plPropNum)
{
	*plPropNum = -1;
	CAtlString csPropName = OLE2T(bstrPropName);

	if (TermString(IDS_TERM_ENABLED, 0) == csPropName)
	{
		*plPropNum = 0;
	}
	else if (TermString(IDS_TERM_ENABLED, 1) == csPropName)
	{
		*plPropNum = 0;
	}
	if (TermString(IDS_ERROR_LOG, 0) == csPropName)
	{
		*plPropNum = 1;
	}
	else if (TermString(IDS_ERROR_LOG, 1) == csPropName)
	{
		*plPropNum = 1;
	}

	return (*plPropNum == -1) ? S_FALSE : S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetPropName(long lPropNum, long lPropAlias,
	BSTR *pbstrPropName)
{
	CAtlString csPropName = L"";

	switch (lPropNum)
	{
	case ePropIsEnabled:
		csPropName = TermString(IDS_TERM_ENABLED, lPropAlias);
		*pbstrPropName = csPropName.AllocSysString();
		break;
	default:
		*pbstrPropName = csPropName.AllocSysString();
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetPropVal(long lPropNum, VARIANT *pvarPropVal)
{
	::VariantInit(pvarPropVal);

	switch (lPropNum)
	{
	case ePropIsEnabled:
		V_VT(pvarPropVal) = VT_I4;
		V_I4(pvarPropVal) = m_boolEnabled ? 1 : 0;
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::SetPropVal(long lPropNum, VARIANT *pvarPropVal)
{
	switch (lPropNum)
	{
	case ePropIsEnabled:
		if (V_VT(pvarPropVal) != VT_I4)
			return S_FALSE;
		m_boolEnabled = V_I4(pvarPropVal) ? 1 : 0;
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::IsPropReadable(long lPropNum, BOOL *pboolPropRead)
{
	switch (lPropNum)
	{
	case ePropIsEnabled:
		*pboolPropRead = TRUE;
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::IsPropWritable(long lPropNum, BOOL *pboolPropWrite)
{
	switch (lPropNum)
	{
	case ePropIsEnabled:
		*pboolPropWrite = TRUE;
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetNMethods(long *plMethods)
{
	*plMethods = eMethLast;

	if (this->m_iStatusLine)
		m_iStatusLine->SetStatusLine(L"Query NMethods");

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::FindMethod(BSTR bstrMethodName, long *plMethodNum)
{
	USES_CONVERSION;

	*plMethodNum = -1;
	CAtlString csPropName = OLE2T(bstrMethodName);

	if (TermString(IDS_TERM_ENABLE, 0) == csPropName)
	{
		*plMethodNum = 0;
	}
	else if (TermString(IDS_TERM_ENABLE, 1) == csPropName)
	{
		*plMethodNum = 0;
	}
	if (TermString(IDS_QUERY_EXECUTE, 0) == csPropName)
	{
		*plMethodNum = 1;
	}
	else if (TermString(IDS_QUERY_EXECUTE, 1) == csPropName)
	{
		*plMethodNum = 1;
	}
	if (TermString(IDS_PROP_PATH, 0) == csPropName)
	{
		*plMethodNum = 2;
	}
	else if (TermString(IDS_PROP_PATH, 1) == csPropName)
	{
		*plMethodNum = 2;
	}
	if (TermString(IDS_INITILIZE, 0) == csPropName)
	{
		*plMethodNum = 3;
	}
	else if (TermString(IDS_INITILIZE, 1) == csPropName)
	{
		*plMethodNum = 3;
	}
	if (TermString(IDS_REGISTRATION, 0) == csPropName)
	{
		*plMethodNum = 4;
	}
	else if (TermString(IDS_REGISTRATION, 1) == csPropName)
	{
		*plMethodNum = 4;
	}
	if (TermString(IDS_REQUEST_FILE_MAKE, 0) == csPropName)
	{
		*plMethodNum = 5;
	}
	else if (TermString(IDS_REQUEST_FILE_MAKE, 1) == csPropName)
	{
		*plMethodNum = 5;
	}
	if (TermString(IDS_ACTIVATION, 0) == csPropName)
	{
		*plMethodNum = 6;
	}
	else if (TermString(IDS_ACTIVATION, 1) == csPropName)
	{
		*plMethodNum = 6;
	}
	if (TermString(IDS_LIC_INFO, 0) == csPropName)
	{
		*plMethodNum = 7;
	}
	else if (TermString(IDS_LIC_INFO, 1) == csPropName)
	{
		*plMethodNum = 7;
	}
	if (TermString(IDS_ERROR_LOG, 0) == csPropName)
	{
		*plMethodNum = 8;
	}
	else if (TermString(IDS_ERROR_LOG, 1) == csPropName)
	{
		*plMethodNum = 8;
	}
	if (TermString(IDS_INFORMATION, 0) == csPropName)
	{
		*plMethodNum = 9;
	}
	else if (TermString(IDS_INFORMATION, 1) == csPropName)
	{
		*plMethodNum = 9;
	}
	if (TermString(IDS_DONE, 0) == csPropName)
	{
		*plMethodNum = 10;
	}
	else if (TermString(IDS_DONE, 1) == csPropName)
	{
		*plMethodNum = 10;
	}
	if (TermString(IDS_LIC_VIEW, 0) == csPropName)
	{
		*plMethodNum = 11;
	}
	else if (TermString(IDS_LIC_VIEW, 1) == csPropName)
	{
		*plMethodNum = 11;
	}
	if (TermString(IDS_FIND_SERVER, 0) == csPropName)
	{
		*plMethodNum = 12;
	}
	else if (TermString(IDS_FIND_SERVER, 1) == csPropName)
	{
		*plMethodNum = 12;
	}
	if (TermString(IDS_SET_IP, 0) == csPropName)
	{
		*plMethodNum = 13;
	}
	else if (TermString(IDS_SET_IP, 1) == csPropName)
	{
		*plMethodNum = 13;
	}
	return (*plMethodNum == -1) ? S_FALSE : S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetMethodName(long lMethodNum, long lMethodAlias,
	BSTR *pbstrMethodName)
{

	CAtlString csMethodName;

	switch (lMethodNum)
	{
	case eMethEnable:
		csMethodName = TermString(IDS_TERM_ENABLE, lMethodAlias);
		break;
	case eMethQuery:
		csMethodName = TermString(IDS_QUERY_EXECUTE, lMethodAlias);
		break;
	case eMethPathSet:
		csMethodName = TermString(IDS_PROP_PATH, lMethodAlias);
		break;
	case eMethInitCOM:
		csMethodName = TermString(IDS_INITILIZE, lMethodAlias);
		break;
	case eMethRegistration:
		csMethodName = TermString(IDS_REGISTRATION, lMethodAlias);
		break;
	case eMethRequestFileMake:
		csMethodName = TermString(IDS_REQUEST_FILE_MAKE, lMethodAlias);
		break;
	case eMethActivate:
		csMethodName = TermString(IDS_ACTIVATION, lMethodAlias);
		break;
	case eMethLicInfo:
		csMethodName = TermString(IDS_LIC_INFO, lMethodAlias);
		break;
	case eMethErrorLog:
		csMethodName = TermString(IDS_ERROR_LOG, lMethodAlias);
		break;
	case eMethUInfo:
		csMethodName = TermString(IDS_INFORMATION, lMethodAlias);
		break;
	case eMethDone:
		csMethodName = TermString(IDS_DONE, lMethodAlias);
		break;
	case eMethView:
		csMethodName = TermString(IDS_DONE, lMethodAlias);
		break;
	case eMethFindServer:
		csMethodName = TermString(IDS_FIND_SERVER, lMethodAlias);
		break;
	case eMethSetIP:
		csMethodName = TermString(IDS_SET_IP, lMethodAlias);
		break;
	default:
		*pbstrMethodName = SysAllocString(csMethodName);
		return S_FALSE;
	}

	*pbstrMethodName = ::SysAllocString(csMethodName);

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetNParams(long lMethodNum, long *plParams)
{
	*plParams = 0;

	switch (lMethodNum)
	{
	case eMethEnable:
		*plParams = 0;
		break;
	case eMethQuery:
		*plParams = 2;
		break;
	case eMethPathSet:
		*plParams = 1;
		break;
	case eMethInitCOM:
		*plParams = 0;
		break;
	case eMethRegistration:
		*plParams = 2;
		break;
	case eMethRequestFileMake:
		*plParams = 1;
		break;
	case eMethActivate:
		*plParams = 1;
		break;
	case eMethLicInfo:
		*plParams = 0;
		break;
	case eMethErrorLog:
		*plParams = 0;
		break;
	case eMethUInfo:
		*plParams = 0;
		break;
	case eMethDone:
		*plParams = 0;
		break;
	case eMethView:
		*plParams = 1;
		break;
	case eMethFindServer:
		*plParams = 1;
		break;
	case eMethSetIP:
		*plParams = 1;
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::GetParamDefValue(long lMethodNum, long lParamNum,
	VARIANT *pvarParamDefValue)
{
	::VariantInit(pvarParamDefValue);

	switch (lMethodNum)
	{
	case eMethEnable:
	case eMethQuery:
		/* There are no parameter values by default */
		break;
	case eMethPathSet:
		break;
	case eMethInitCOM:
		break;
	case eMethRegistration:
		break;
	case eMethRequestFileMake:
		break;
	case eMethActivate:
		break;
	case eMethLicInfo:
		break;
	case eMethErrorLog:
		break;
	case eMethUInfo:
		break;
	case eMethDone:
		break;
	case eMethView:
		break;
	case eMethFindServer:
		break;
	case eMethSetIP:
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::HasRetVal(long lMethodNum, BOOL *pboolRetValue)
{
	switch (lMethodNum)
	{
	case eMethEnable:
		*pboolRetValue = FALSE;
		break;
	case eMethQuery:
		*pboolRetValue = TRUE;
		break;
	case eMethPathSet:
		*pboolRetValue = FALSE;
		break;
	case eMethInitCOM:
		*pboolRetValue = TRUE;
		break;
	case eMethRegistration:
		*pboolRetValue = TRUE;
		break;
	case eMethRequestFileMake:
		*pboolRetValue = TRUE;
		break;
	case eMethActivate:
		*pboolRetValue = TRUE;
		break;
	case eMethLicInfo:
		*pboolRetValue = TRUE;
		break;
	case eMethErrorLog:
		*pboolRetValue = TRUE;
		break;
	case eMethUInfo:
		*pboolRetValue = TRUE;
		break;
	case eMethDone:
		*pboolRetValue = TRUE;
		break;
	case eMethView:
		*pboolRetValue = TRUE;
		break;
	case eMethFindServer:
		*pboolRetValue = TRUE;
		break;
	case eMethSetIP:
		*pboolRetValue = TRUE;
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::CallAsProc(long lMethodNum, SAFEARRAY **paParams)
{
	USES_CONVERSION;

	switch (lMethodNum)
	{
	case eMethEnable:
		break;
	case eMethQuery:
		break;
	case eMethPathSet:
		if (!cmfc.FindPathQFile(paParams))
		{
			Messenger("������! ��������� ������� �� ��������� �� ������ ����� ��������!");
		}
		break;
	case eMethInitCOM:
		break;
	case eMethActivate:
		break;
	case eMethLicInfo:
		break;
	case eMethUInfo:
		break;
	case eMethFindServer:
		break;
	default:
		return S_FALSE;
	}

	return S_OK;
}
//---------------------------------------------------------------------------//
STDMETHODIMP CAddInCOM::CallAsFunc(long lMethodNum, VARIANT *pvarRetValue,
	SAFEARRAY **paParams)
{
	switch (lMethodNum)
	{
	case eMethQuery:
	{
					   cmfc.errorCode = 0;
					   CComVariant vResult;
					   vResult = QueryMeth(paParams);
					   if (V_VT(&vResult) == VT_BOOL)
					   {
						   ::VariantInit(&vResult);
						   V_VT(pvarRetValue) = VT_DISPATCH;
						   V_DISPATCH(pvarRetValue) = V_DISPATCH(&vResult);
						   break;
					   }
					   V_VT(pvarRetValue) = VT_DISPATCH;
					   V_DISPATCH(pvarRetValue) = V_DISPATCH(&vResult);
					   break;
	}
	case eMethInitCOM:
	{
						 CComVariant vResult;
						 vResult.intVal=Ini();
						 V_VT(pvarRetValue) = VT_BOOL;
						 V_BOOL(pvarRetValue) = V_BOOL(&vResult);
						 break;
	}
	case eMethRegistration:
	{
							  CComVariant vResult;
							  vResult.boolVal = RegistrateUser(paParams);
							  V_VT(pvarRetValue) = VT_BOOL;
							  V_BOOL(pvarRetValue) = V_BOOL(&vResult);
							  break;
	}
	case eMethRequestFileMake:
	{
								 CComVariant vResult;
								 int i =RequestMake(paParams);
								 vResult.intVal = i;
								 V_VT(pvarRetValue) = VT_I4;
								 V_I4(pvarRetValue) = V_I4(&vResult);
								 break;
	}
	case eMethActivate:
	{
						  CComVariant vResult;
						  bool set = Activ(paParams);
						  vResult.boolVal = set;
						  V_VT(pvarRetValue) = VT_BOOL;
						  V_BOOL(pvarRetValue) = V_BOOL(&vResult);
						  break;
	}
	case eMethLicInfo:
	{
						 CComVariant vResult;
						 vResult = licenseInformation();
						 V_VT(pvarRetValue) = VT_DISPATCH;
						 V_DISPATCH(pvarRetValue) = V_DISPATCH(&vResult);
						 break;
	}
	case eMethErrorLog:
	{
						  CComVariant vResult;
						  vResult = ErrorLog();
						  V_VT(pvarRetValue) = VT_DISPATCH;
						  V_DISPATCH(pvarRetValue) = V_DISPATCH(&vResult);
						  break;
	}
	case eMethUInfo:
	{
					   CComVariant vResult;
					   vResult = UInformation();
					   V_VT(pvarRetValue) = VT_DISPATCH;
					   V_DISPATCH(pvarRetValue) = V_DISPATCH(&vResult);
					   break;
	}
	case eMethDone:
	{
					  CComVariant vResult;
					  vResult.boolVal = DoneConnect();
					  V_VT(pvarRetValue) = VT_BOOL;
					  V_BOOL(pvarRetValue) = V_BOOL(&vResult);
					  break;
	}
	case eMethView:
	{
					  CComVariant vResult;
					  vResult = SrvView(paParams);
					  V_VT(pvarRetValue) = VT_DISPATCH;
					  V_DISPATCH(pvarRetValue) = V_DISPATCH(&vResult);
					  break;
	}
	case eMethFindServer:
	{
							CComVariant vResult;
							vResult.boolVal = FindServer(paParams);
							V_VT(pvarRetValue) = VT_BOOL;
							V_BOOL(pvarRetValue) = V_BOOL(&vResult);
							break;
	}
	case eMethSetIP:
	{
					   CComVariant vResult;
					   vResult = ServerSet(paParams);
					   V_VT(pvarRetValue) = VT_BOOL;
					   V_BOOL(pvarRetValue) = V_BOOL(&vResult);
					   break;
	}
	default:
	{
			   ::VariantInit(pvarRetValue);
			   break;
	}
	}

	return S_OK;;
}
//---------------------------------------------------------------------------//
CAtlStringW CAddInCOM::TermString(UINT uiResID, long nAlias)
{
	USES_CONVERSION;
	CAtlStringA cs;
	cs.LoadString(uiResID);

	int iInd = cs.Find(',');
	if (iInd == -1)
		return CAtlString(cs);
	switch (nAlias)
	{
	case 0: // First language
		return CAtlString(cs.Left(iInd));
	case 1: // Second language
		return CAtlString(cs.Mid(iInd + 1));
	default:
		return CAtlString(L"");
	};
}
//---------------------------------------------------------------------------//
VARIANT CAddInCOM::GetNParam(SAFEARRAY *pArray, long lIndex)
{
	_ASSERT(pArray);
	_ASSERT(pArray->fFeatures | FADF_VARIANT);

	VARIANT vt;
	HRESULT hRes = ::SafeArrayGetElement(pArray, &lIndex, &vt);
	_ASSERT(hRes == S_OK);

	return vt;
}
//---------------------------------------------------------------------------//
HRESULT CAddInCOM::SetLocale(BSTR bstrLocale)
{
	return S_OK;
}
//---------------------------------------------------------------------------//

bool CAddInCOM::ServerSet(SAFEARRAY **paParams)
{

	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vParam;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam);
	if (V_VT(&vParam) == VT_BSTR)
	{
		char* ip_address = _com_util::ConvertBSTRToString(vParam.bstrVal);
		if (inet_addr(ip_address) != INADDR_NONE)
		{
			TCHAR szPath[MAX_PATH];
			std::string path;
			bool is;
			if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS | CSIDL_FLAG_CREATE, NULL, 0, &szPath[0])))
			{
				PathAppend(szPath, TEXT("RBSoft"));
				if (!PathIsDirectory(szPath))
				{
					is = CreateDirectory(szPath, 0);
				}
				PathAppend(szPath, TEXT("RBSoft1CExtension"));
				if (!PathIsDirectory(szPath))
				{
					is = CreateDirectory(szPath, 0);
				}
				PathAppend(szPath, TEXT("data"));
				if (!PathIsDirectory(szPath))
				{
					is = CreateDirectory(szPath, 0);
				}
				PathAppend(szPath, TEXT("\\serv_ip.cnf"));
				std::wstring str(szPath);
				path.resize(str.size());
				std::transform(str.begin(), str.end(), path.begin(), wctob);
			}
			const char* pathh = path.c_str();
			net_client.server_addr.sin_addr.s_addr = inet_addr(ip_address);
			net_client.server_addr.sin_family = AF_INET;
			net_client.server_addr.sin_port = htons(2512);
			FILE *out;
			char buff[1024];
			try
			{
				memcpy(&buff[0], &net_client.server_addr, sizeof(net_client.server_addr));
				fopen_s(&out, pathh, "wb");
				fwrite(&buff[0], sizeof(net_client.server_addr), 1, out);
				fclose(out);
			}
			catch (HRESULT hh)
			{
				cmfc.my_info.errorCode = 9001;
				char* c_tmp = "������: ������ ���������� ������������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				return false;
			}
			return true;
		}
	}
	cmfc.my_info.errorCode = 9002;
	char* c_tmp = "������: ���������� �������� �� �������� ������� IPv4";
	memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
	return false;
}

bool CAddInCOM::FindServer(SAFEARRAY **paParams)
{
	net_client.serverList.clear();
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vApp, vResult, vTmp,vParam;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam);
	int retVal = vParam.iVal;
	if (net_client.serverMap(m_iConnect,1))
	{
		if (net_client.serverList.size() <= 0)
		{
			Messenger("�� ������ �� ���� ���������� ������ ��������!");
			cmfc.my_info.errorCode = 9101;
			char* c_tmp = "������: ��� ��������� �������� ��������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
			return false;
		}
		else
		{
			TCHAR szPath[MAX_PATH];
			std::string path;
			bool is;
			if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS | CSIDL_FLAG_CREATE, NULL, 0, &szPath[0])))
			{
				PathAppend(szPath, TEXT("RBSoft"));
				if (!PathIsDirectory(szPath))
				{
					is = CreateDirectory(szPath, 0);
				}
				PathAppend(szPath, TEXT("RBSoft1CExtension"));
				if (!PathIsDirectory(szPath))
				{
					is = CreateDirectory(szPath, 0);
				}
				PathAppend(szPath, TEXT("data"));
				if (!PathIsDirectory(szPath))
				{
					is = CreateDirectory(szPath, 0);
				}
				PathAppend(szPath, TEXT("\\serv_ip.cnf"));
				std::wstring str(szPath);
				path.resize(str.size());
				std::transform(str.begin(), str.end(), path.begin(), wctob);
			}
			const char* pathh = path.c_str();
			net_client.server_addr.sin_addr.s_addr = net_client.serverList[0].sin_addr.s_addr;
			net_client.server_addr.sin_family = AF_INET;
			net_client.server_addr.sin_port = htons(2512);
			FILE *out;
			char buff[1024];
			try
			{
				memcpy(&buff[0], &net_client.server_addr, sizeof(net_client.server_addr));
				fopen_s(&out, pathh, "wb");
				fwrite(&buff[0], sizeof(net_client.server_addr), 1, out);
				fclose(out);
			}
			catch (HRESULT hh)
			{
				cmfc.my_info.errorCode = 5010;
				char* c_tmp = "������: �� ������� ������������ � �������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				return false;
			}
			return true;
		}
	}
	else
	{
		cmfc.my_info.errorCode = 9100;
		char* c_tmp = "������: �� ������� ���������������� ����";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
}



VARIANT CAddInCOM::SrvView(SAFEARRAY **paParams)
{
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vParam;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam);
	CComVariant vApp, vResult, vTmp,vClient[20];
	if (V_VT(&vParam) == VT_I4)
	{
		int comm = vParam.iVal;
#pragma region local_lic_list
		if (comm == 0)
		{
			//local license list
			try
			{
				net_client.in_size = 0;
				net_client.command = NET_GET_LICENSE_LIST;
			}
			catch (HRESULT hh)
			{
				cmfc.my_info.errorCode = 7000;
				char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}
			if (!net_client.net_msg())
			{
				cmfc.my_info.errorCode = 7110;
				char* c_tmp = "������: �� ������� �������� ����������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}
			else
			{
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(0), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t("�������"), &vTmp);
					int count;
					memcpy(&count, &net_client.out_msg[0], sizeof(int));
					hr = pResult.Invoke2(L"��������", &variant_t(L"����������"), &variant_t(count), &vTmp);
					for (int i = 0; i < count;i++)
					{
						char tmp[32];
						memset(&tmp[0],0,sizeof(tmp));
						int seek = sizeof(int)+(i * 32);
						memcpy(&tmp[0], &net_client.out_msg[seek], 32);
						hr = pResult.Invoke2(L"��������", &variant_t(i), &variant_t(tmp), &vTmp);
					}
				}
				pResult.Detach();
				return vResult;
			}
		}
#pragma endregion �������� ���������� �������
#pragma region serverMap
		else if (comm == 1)
		{
			//server list
			if (net_client.serverMap(m_iConnect,0))
			{
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(0), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t("�������"), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"����������"), &variant_t(net_client.serverList.size()), &vTmp);
					int k=0;
					for each (sockaddr_in var in net_client.serverList)
					{
						hr = pResult.Invoke2(L"��������", &variant_t(k), &variant_t(inet_ntoa(var.sin_addr)), &vTmp);
					}
				}
				pResult.Detach();
				return vResult;
			}
			else
			{
				cmfc.my_info.errorCode = net_client.error_code;
				char* c_tmp = "������: �� ������� ���������������� ����";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(net_client.error_code), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t("������: �� ������� ���������������� ����"), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}
		}
#pragma endregion ������ �������� � ����
#pragma region error_param_int
		else
		{
			cmfc.my_info.errorCode = 12100;
			char* c_tmp = "������: �� ��������� ���������� ��������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
			hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
			CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
			hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
			CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
			if (SUCCEEDED(hr))
			{
				hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
				hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
			}
			pResult.Detach();
			return vResult;
		}
#pragma endregion ������������ ������� �����
	}
	else if (V_VT(&vParam) == VT_BSTR)
	{
		char* inParam = _com_util::ConvertBSTRToString(vParam.bstrVal);
#pragma region client_info
		if (inet_addr(inParam) == INADDR_NONE)
		{
			std::tr1::cmatch cmt;
			std::tr1::regex rx("([^,]+)");
			std::tr1::regex_search(inParam, cmt, rx);
			std::string prod = cmt[0];
			std::string ip = cmt[1].second;
			const char* tmp = prod.c_str();
			SHPCK pd;
			USEPCK up;
			try
			{
				memset(&pd.product[0], 0, sizeof(pd.product));
				memcpy(&pd.product[0], &tmp[0], strlen(tmp));
				memcpy(&net_client.in_msg[0], &pd, sizeof(pd));
				net_client.in_size = sizeof(pd);
				net_client.command = NET_GET_USER_LIST;
				tmp = ip.c_str();
				memcpy(&net_client.inLoad[0], &tmp[1], strlen(tmp));
				net_client.loadAddr = true;
			}
			catch (HRESULT hh)
			{
				cmfc.my_info.errorCode = 7000;
				char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}
			if (!net_client.net_msg())
			{
				cmfc.my_info.errorCode = net_client.error_code;
				char* c_tmp = "������: ������ �������� ������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}
			else
			{
				char* out_data = new char[1512];
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				memcpy(&up, &net_client.out_msg[0], sizeof(up));
				if (up.lic_count <= 0)
				{
					cmfc.my_info.errorCode = 0;
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(0), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"������������"), &variant_t(0), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					pResult.Detach();
					return vResult;
				}
				else
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(up.lic_count), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"������������"), &variant_t(up.in_use), &vTmp);
					for (int i = 0; i < up.in_use; i++)
					{
						hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vClient[i]);
						CComPtr<IDispatch>pClient = V_DISPATCH(&vClient[i]);
						hr = pClient.Invoke2(L"��������", &variant_t("�����"), &variant_t(inet_ntoa(up.adr[i].ip_info.sin_addr)), &vTmp);
						//hr = pClient.Invoke2(L"��������", &variant_t("����"), &variant_t(up.adr[i].hst.h_name), &vTmp);
						char* tts = asctime(&up.adr[i].connect_time);
						hr = pClient.Invoke2(L"��������", &variant_t("���������"), &variant_t(tts), &vTmp);
						hr = pResult.Invoke2(L"��������", &variant_t(i), &vClient[i], &vTmp);
						pClient.Detach();
					}
					pResult.Detach();
					return vResult;
				}
			}
		}
#pragma endregion ���������� � ����������� ��������
#pragma region lic_info_from_addr
		else
		{
			try
			{
				net_client.loadAddr = true;
				memset(&net_client.inLoad[0], 0, sizeof(net_client.inLoad));
				memcpy(&net_client.inLoad[0], &inParam[0], strlen(inParam));
				net_client.in_size = 0;
				net_client.command = NET_GET_LICENSE_LIST;
			}
			catch (HRESULT HH)
			{
				cmfc.my_info.errorCode = 7120;
				char* c_tmp = "������: �� ������� ���������� ����������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}

			if (!net_client.net_msg())
			{
				cmfc.my_info.errorCode = net_client.error_code;
				char* c_tmp = "������: �� ������� �������� ����������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
				}
				pResult.Detach();
				return vResult;
			}
			else
			{
				hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
				CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
				hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
				CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
				if (SUCCEEDED(hr))
				{
					hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(0), &vTmp);
					hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t("�������"), &vTmp);
					int count;
					memcpy(&count, &net_client.out_msg[0], sizeof(int));
					hr = pResult.Invoke2(L"��������", &variant_t(L"����������"), &variant_t(count), &vTmp);
					for (int i = 0; i < count; i++)
					{
						char tmp[32];
						memset(&tmp[0], 0, sizeof(tmp));
						int seek = sizeof(int)+(i * 32);
						memcpy(&tmp[0], &net_client.out_msg[seek], 32);
						hr = pResult.Invoke2(L"��������", &variant_t(i), &variant_t(tmp), &vTmp);
					}
				}
				pResult.Detach();
				return vResult;
			}
		}
#pragma endregion ���������� � ��������� �� ������
	}
	else
	{
		cmfc.my_info.errorCode = 7130;
		char* c_tmp = "������: �� ����� ���������� ��������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
		CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
		hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
		CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
		if (SUCCEEDED(hr))
		{
			hr = pResult.Invoke2(L"��������", &variant_t(L"������"), &variant_t(cmfc.my_info.errorCode), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(c_tmp), &vTmp);
		}
		pResult.Detach();
		return vResult;
	}
}



bool CAddInCOM::DoneConnect()
{
	init_lic_data idata;
	try
	{
		memcpy(&idata.product[0], &cmfc.my_info.product[0], sizeof(cmfc.nameQF));
		memcpy(&net_client.in_msg[0], &idata, sizeof(idata));
		net_client.in_size = sizeof(idata);
		net_client.command = NET_WORK_END;
	}
	catch (HRESULT hh)
	{
		cmfc.my_info.errorCode = 7000;
		char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
	if (!net_client.net_msg())
	{
		cmfc.my_info.isLicensed = false;
		cmfc.my_info.errorCode = net_client.error_code;
		char* c_tmp = "������: �� ���������� ����� � ������� ��������������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
	else
	{
		cmfc.my_info.isLicensed = false;
		cmfc.my_info.errorCode = net_client.error_code;
		char* c_tmp = "�������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
}



VARIANT CAddInCOM::UInformation()
{
	HRESULT hr;
	CComVariant vApp, vResult, vTmp;
	hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
	CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
	hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
	CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
	if (SUCCEEDED(hr))
	{
		try
		{
			hr = pResult.Invoke2(L"��������", &variant_t(L"���"), &variant_t(cmfc.my_info.lic.reg_data.name), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"�������"), &variant_t(cmfc.my_info.lic.reg_data.fam), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(cmfc.my_info.lic.reg_data.otch), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"�����������"), &variant_t(cmfc.my_info.lic.reg_data.organ), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"�����"), &variant_t(cmfc.my_info.lic.reg_data.adress), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"���"), &variant_t(cmfc.my_info.lic.reg_data.inn), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"��.�����"), &variant_t(cmfc.my_info.lic.reg_data.email), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"�������"), &variant_t(cmfc.my_info.lic.reg_data.tel), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"�����������"), &variant_t(inet_ntoa(net_client.server_addr.sin_addr)), &vTmp);
			if (cmfc.my_info.lic.serial_key > 0)
			{
				hr = pResult.Invoke2(L"��������", &variant_t(L"�����"), &variant_t(cmfc.my_info.lic.serial_key), &vTmp);
			}
			else
			{
				hr = pResult.Invoke2(L"��������", &variant_t(L"�����"), &variant_t(""), &vTmp);
			}
			char* c_tmp = new char[1024];
			char* out_data = new char[1512];
			memset(&c_tmp, 0, sizeof(c_tmp));
			switch (cmfc.my_info.lic.license_type)
			{
			case DEMO:
				c_tmp = "���������������� ������";
				try
				{
					char* tts = "00";
					char* ttt = "01";
					memcpy(&out_data[0], &tts[0], strlen(tts));
					memcpy(&out_data[strlen(tts)], &ttt[0], strlen(ttt));
					int seek = strlen(tts) + strlen(ttt);
					memcpy(&out_data[seek], &ttt[0], strlen(ttt));
					seek = seek + strlen(ttt);
					memcpy(&out_data[seek], &ttt[0], strlen(tts));
					seek = seek + strlen(ttt);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					out_data[seek] = 0;
				}
				catch (HANDLE hh)
				{
					memset(&c_tmp, 0, sizeof(c_tmp));
				}
				break;
			case TR:
				c_tmp = "��������� ��������";
				try
				{
					char* tts = "00";
					char* day = new char[64];
					_itoa_s(cmfc.my_info.lic.date_out.tm_mday, day, 64, 10);
					char* month = new char[64];
					_itoa_s(cmfc.my_info.lic.date_out.tm_mon + 1, month, 64, 10);
					char* year = new char[64];
					_itoa_s(cmfc.my_info.lic.date_out.tm_year, year, 64, 10);
					memcpy(&out_data[0], &year[0], strlen(year));
					if (cmfc.my_info.lic.date_out.tm_mon < 10)
					{
						memcpy(&out_data[strlen(year)], &tts[0], 1);
						memcpy(&out_data[strlen(year) + 1], &month[0], strlen(month));
					}
					else
					{
						memcpy(&out_data[strlen(year)], &month[0], strlen(month));
					}
					int seek = strlen(year) + strlen(month);
					memcpy(&out_data[seek], &day[0], strlen(day));
					seek = seek + strlen(day);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					out_data[seek] = 0;
				}
				catch (HANDLE hh)
				{
					memset(&c_tmp, 0, sizeof(c_tmp));
				}
				break;
			case FL:
				c_tmp = "������ ��������";
				try
				{
					char* tts = "00";
					char* day = new char[64];
					_itoa_s(cmfc.my_info.lic.date_out.tm_mday, day, 64, 10);
					char* month = new char[64];
					_itoa_s(cmfc.my_info.lic.date_out.tm_mon + 1, month, 64, 10);
					char* year = new char[64];
					_itoa_s(cmfc.my_info.lic.date_out.tm_year, year, 64, 10);
					memcpy(&out_data[0], &year[0], strlen(year));
					if (cmfc.my_info.lic.date_out.tm_mon < 10)
					{
						memcpy(&out_data[strlen(year)], &tts[0], 1);
						memcpy(&out_data[strlen(year) + 1], &month[0], strlen(month));
					}
					else
					{
						memcpy(&out_data[strlen(year)], &month[0], strlen(month));
					}
					int seek = strlen(year) + strlen(month)+1;
					memcpy(&out_data[seek], &day[0], strlen(day));
					seek = seek + strlen(day);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					memcpy(&out_data[seek], &tts[0], strlen(tts));
					seek = seek + strlen(tts);
					out_data[seek] = 0;
				}
				catch (HANDLE hh)
				{
					memset(&c_tmp, 0, sizeof(c_tmp));
				}
				break;
			default:
				c_tmp = "������: �������� �� ����������";
				memset(&c_tmp, 0, sizeof(c_tmp));
				break;
			}
			hr = pResult.Invoke2(L"��������", &variant_t(L"���������"), &variant_t(out_data), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"��� ��������"), &variant_t(c_tmp), &vTmp);
			hr = pResult.Invoke2(L"��������", &variant_t(L"������������"), &variant_t(cmfc.my_info.product), &vTmp);
		}
		catch (HRESULT hh)
		{
			Messenger("Fatal error: can not create 1C object. ���������� ����� �������.");
			hr = pApp.Invoke1(L"����������������������", &variant_t(false), NULL);
		}
	}
	pResult.Detach();
	return vResult;
}



VARIANT CAddInCOM::ErrorLog()
{
	HRESULT hr;
	CComVariant vApp, vResult, vTmp;
	hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
	CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
	hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
	CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
	if (SUCCEEDED(hr))
	{
		hr = pResult.Invoke2(L"��������", &variant_t(L"���"), &variant_t(cmfc.my_info.errorCode), &vTmp);
		hr = pResult.Invoke2(L"��������", &variant_t(L"���������"), &variant_t(cmfc.my_info.error_descrypt), &vTmp);
	}
	pResult.Detach();
	return vResult;
}


VARIANT CAddInCOM::licenseInformation()
{
	HRESULT hr;
	CComVariant vApp,vResult,vTmp;
	hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
	CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
	hr = pApp.Invoke1(L"NewObject", &variant_t(L"������������"), &vResult);
	CComPtr<IDispatch>pResult = V_DISPATCH(&vResult);
	if (SUCCEEDED(hr))
	{
		hr = pResult.Invoke2(L"��������", &variant_t(L"�����������"), &variant_t(cmfc.my_info.isRegistrate), &vTmp);
		hr = pResult.Invoke2(L"��������", &variant_t(L"��������"), &variant_t(cmfc.my_info.isLicensed), &vTmp);
	}
	pResult.Detach();
	return vResult;
}



bool CAddInCOM::Activ(SAFEARRAY **paParams)
{ 
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vParam;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam);
	if (V_VT(&vParam) != VT_BSTR)
	{
		cmfc.my_info.errorCode = 7010;
		char* c_tmp = "������: ������ ����������� ���������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
	BSTR tmp = vParam.bstrVal;
	char* ch = _com_util::ConvertBSTRToString(tmp);
	if (!PathFileExistsA(ch))
	{
		cmfc.my_info.errorCode = 11001;
		char* c_tmp = "������: ���� �� ����������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
	else
	{
		FILE *in;
		char buff[4096];
		try
		{
			fopen_s(&in, ch, "rb");
			fread(&buff[0], sizeof(buff), 1, in);
			memset(&net_client.in_msg[0], 0, sizeof(net_client.in_msg));
			memcpy(&net_client.in_msg[0], &buff[0], sizeof(buff));
			net_client.in_size = sizeof(buff);
			net_client.command = NET_ACTIVATE_LICENSE;
		}
		catch (HRESULT hh)
		{
			cmfc.my_info.errorCode = 11002;
			char* c_tmp = "������: ���� �� �������� ��� ������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
		if (!net_client.net_msg())
		{
			cmfc.my_info.errorCode = 11003;
			char* c_tmp = "������: �� ������� ������� ��������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
		else
		{
			int error;
			try
			{
				memcpy(&error, &net_client.out_msg[0], sizeof(int));
			}
			catch (HRESULT hh)
			{
				cmfc.my_info.errorCode = 11004;
				char* c_tmp = "������: �� ������� ������� ��������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
				return false;
			}
			if (error == LICENSE_ACTIVATED)
			{
				cmfc.my_info.errorCode = 0;
				char* c_tmp = "�������� ������������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
				return true;
			}
			else if (error == LICENSE_ACTIVATION_ERROR_NET_ERROR)
			{
				cmfc.my_info.errorCode = 11005;
				char* c_tmp = "������: �� ������� �������� ��������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
				return false;
			}
			else if (error == LICENSE_ACTIVATION_ERROR_NOT_VALID_MACHINE)
			{
				cmfc.my_info.errorCode = 11006;
				char* c_tmp = "������: ������ �������� ��  ���������";
				memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
				return false;
			}
		}
	}
	return true;
}



bool CAddInCOM::RequestMake(SAFEARRAY **paParams)
{
	cmfc.my_info.errorCode = 0;
	memset(&cmfc.my_info.error_descrypt[0], 0, sizeof(cmfc.my_info.error_descrypt));
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vParam2;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam2);
	if (V_VT(&vParam2) != VT_BSTR)
	{
		cmfc.my_info.errorCode = 7010;
		char* c_tmp = "������: ������ ����������� ���������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
	BSTR tmp = vParam2.bstrVal;
	char* ch = _com_util::ConvertBSTRToString(tmp);
	if (ch == NULL)
	{
		cmfc.my_info.errorCode = 7011;
		char* c_tmp = "������: �������� �� ������������������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
	try
	{
		memset(&net_client.in_msg[0], 0, sizeof(net_client.in_msg));
		net_client.in_size = 0;
		net_client.command = NET_GET_HARDWARE_ID;
	}
	catch (HRESULT hh)
	{
		cmfc.my_info.errorCode = 7000;
		char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
	if (!net_client.net_msg())
	{
		cmfc.my_info.errorCode = net_client.error_code;
		char* c_tmp = "������: �� ���������� ����� � ������� ��������������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
	else
	{
		memset(&cmfc.my_info.lic.reg_data.loc_mahine[0], 0, sizeof(cmfc.my_info.lic.reg_data.loc_mahine));
		memcpy(&cmfc.my_info.lic.reg_data.loc_mahine[0], &net_client.out_msg[0], sizeof(cmfc.my_info.lic.reg_data.loc_mahine));
	}
	try
	{
		std::string path(ch);
		std::string info;
		std::ofstream out(path, std::ofstream::binary);
		info = cmfc.my_info.lic.reg_data.name;
		out << "<name>" << info << "</name>" << "\n";
		info = cmfc.my_info.lic.reg_data.fam;
		out << "<lastname>" << info << "</lastname>" << std::endl;
		info = cmfc.my_info.lic.reg_data.otch;
		out << "<parname>" << info << "</parname>" << std::endl;
		info = cmfc.my_info.lic.reg_data.organ;
		out << "<organiz>" << info << "</organiz>" << std::endl;
		info = cmfc.my_info.lic.reg_data.inn;
		out << "<inn>" << info << "</inn>" << std::endl;
		info = cmfc.my_info.lic.reg_data.email;
		out << "<email>" << info << "</email>" << std::endl;
		info = cmfc.my_info.lic.reg_data.adress;
		out << "<adress>" << info << "</adress>" << std::endl;
		info = cmfc.my_info.lic.reg_data.tel;
		out << "<telnum>" << info << "</telnum>" << std::endl;
		info = cmfc.my_info.product;
		out << "<prod>" << info << "</prod>" << std::endl;
		char it[32];
		memcpy(&it[0], &cmfc.my_info.lic.reg_data.loc_mahine[0], sizeof(it));
		out << "<hf1>" << it << "</hf1>" << std::endl;
		out.close();
	}
	catch (HRESULT hh)
	{
		cmfc.my_info.errorCode = 7012;
		char* c_tmp = "������: ������ ��� ������ ���������������� �����";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
	return true;
}



bool CAddInCOM::RegistrateUser(SAFEARRAY **paParams)
{
	if (cmfc.GetUserInfo(paParams))
	{
		return true;
	}
	else
	{
		return false;
	}
}



VARIANT CAddInCOM::QueryMeth(SAFEARRAY **paParams)
{
	if ((cmfc.dType != FL) && (cmfc.dType != TR)&&(cmfc.dType!=DEMO))
	{
		cmfc.my_info.errorCode = 7016;
		char* c_tmp = "������: �������� �� ����������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		CComVariant vResult;
		V_VT(&vResult) = VT_BOOL;
		vResult.boolVal = false;
		return vResult;
	}
	std::string query = cmfc.GetQuery(paParams);
	if (query=="")
	{
		cmfc.my_info.errorCode = cmfc.errorCode;
		char* c_tmp = "������: ���� ������� ��������� ��� ����";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		CComVariant vResult;
		vResult.boolVal = false;
		::VariantInit(&vResult);
		return vResult;
	}
	int i = cmfc.GetParams(query,paParams);
	if (i < 0)
	{ 
		cmfc.my_info.errorCode = cmfc.errorCode;
		char* c_tmp = "������: ����������� �������� ���������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		CComVariant vResult;
		vResult.boolVal = false;
		::VariantInit(&vResult);
		return vResult;
	}
	CComVariant vResult;
	::VariantInit(&vResult);
	vResult = cmfc.Execute(query,m_iConnect);
	cmfc.my_info.errorCode = cmfc.errorCode;
	char* c_tmp = "";
	memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
	return vResult;
}
//---------------------------------------------------------------------------//



bool CAddInCOM::Ini()
{
	cmfc.dType = DEMO;
	cmfc.my_info.errorCode = 0;
	memset(&cmfc.my_info.error_descrypt[0], 0, sizeof(cmfc.my_info.error_descrypt));
#pragma region query_file_check
	if (cmfc.fPath == "")
	{
		if (!cmfc.FindQFile(""))
		{
			cmfc.dType = 0;
			cmfc.my_info.errorCode = 7001;
			char* c_tmp = "������: ���� ������� �� ������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
			return false;
		}
	}
	if (!cmfc.Controling())
	{
		cmfc.dType = 0;
		cmfc.my_info.errorCode = 7002;
		char* c_tmp = "������: �� ��������� ���� �������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
#pragma endregion ������������� ����� ��������
#pragma region license check
	init_lic_data idata;
	try
	{
		memcpy(&idata.product[0], &cmfc.my_info.product[0], sizeof(cmfc.nameQF));
		memcpy(&net_client.in_msg[0], &idata, sizeof(idata));
		net_client.in_size = sizeof(idata);
		net_client.command = NET_INITIALIZE;
	}
	catch (HRESULT hh)
	{
		cmfc.my_info.errorCode = 7000;
		char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
	if (!net_client.net_msg())
	{
		cmfc.my_info.isLicensed = false;
		cmfc.my_info.errorCode = net_client.error_code;
		char* c_tmp;
		c_tmp = "������: ��� ����� � ��������";
		memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
		return false;
	}
	else
	{
		try
		{
			cmfc.my_info.errorCode = net_client.error_code;
			memcpy(&cmfc.my_info.lic, &net_client.out_msg[0], sizeof(lic_data));
			cmfc.dType = cmfc.my_info.lic.license_type;
			cmfc.my_info.isLicensed = net_client.flag;
			if (net_client.flag == true)
			{
				cmfc.my_info.isRegistrate = true;
			}
		}
		catch (HANDLE hh)
		{
			cmfc.my_info.errorCode = 7003;
			char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
			return false;
		}
	}
#pragma endregion �������� ��������
#pragma region user_check
	TCHAR szPath[MAX_PATH];
	std::string path;
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS | CSIDL_FLAG_CREATE, NULL, 0, &szPath[0])))
	{
		PathAppend(szPath, TEXT("RBSoft"));
		if (!PathIsDirectory(szPath))
		{
			CreateDirectory(szPath, 0);
		}
		PathAppend(szPath, TEXT("RBSoft1CExtension"));
		if (!PathIsDirectory(szPath))
		{
			CreateDirectory(szPath, 0);
		}
		PathAppend(szPath, TEXT("data"));
		if (!PathIsDirectory(szPath))
		{
			CreateDirectory(szPath, 0);
		}
		PathAppend(szPath, TEXT("\\ui.cnf"));
		std::wstring str(szPath);
		path.resize(str.size());
		std::transform(str.begin(), str.end(), path.begin(), wctob);
	}
	const char* pathh = path.c_str();
	if (!PathFileExistsA(pathh))
	{
		cmfc.my_info.isRegistrate = false;
	}
	else
	{
		cmfc.my_info.isRegistrate = true;
		FILE *in;
		char buff[1024];
		try
		{
			fopen_s(&in, pathh, "rb");
			fread(&buff[0], sizeof(reg_user_list), 1, in);
			fclose(in);
			memcpy(&cmfc.my_info.lic.reg_data, &buff[0], sizeof(reg_user_list));
		}
		catch (HRESULT hh)
		{
			cmfc.my_info.errorCode = 7004;
			char* c_tmp = "������: �� ���������� ������ ��� �������� ������";
			memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
			return false;
		}
	}
#pragma endregion �������� �����������
	//cmfc.my_info.errorCode = 0;
	char* c_tmp = "�������";
	memcpy(&cmfc.my_info.error_descrypt[0], &c_tmp[0], strlen(c_tmp));
	return true;
}

//---------------------------------------------------------------------------//

bool CAddInCOM::Messenger(std::string msg)
{
	const char* cmsg = msg.c_str();
	HRESULT hr;
	CComVariant vApp;
	hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
	CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
	hr = pApp.Invoke1(L"��������������", &variant_t(cmsg),  NULL);
	return true;
}