
#include "stdafx.h"
#include "COMFuncs.h"





bool COMFunc::GetQText(SAFEARRAY **paParams)
{
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vQText;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vQText);
	if (SUCCEEDED(hr))
	{
		std::string sText;
		BSTR tmp = vQText.bstrVal;
		if (BstrToStdString(tmp, sText) == false)
		{
			errorCode = 2072;
			return false;
		}
		sText.resize(sText.size() - 1);
		if (sText == "")
		{
			errorCode = 2073;
			return false;
		}
		eFile = sText;
		return true;
	}
	else
	{
		errorCode = 2071;
		return false;
	}
}

bool COMFunc::FindPathQFile(SAFEARRAY **paParams)
{
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vPath;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vPath);
	if (SUCCEEDED(hr))
	{
		std::string sPath;
		BSTR tmp = vPath.bstrVal;
		if (BstrToStdString(tmp, sPath) == false)
		{
			errorCode = 2064;
			return false;
		}
		sPath.resize(sPath.size() - 1);
		HANDLE hFind;
		WIN32_FIND_DATA FindFileData;
		std::string path = sPath + "*.qrs";
		std::wstring ws;
		ws.assign(path.begin(), path.end());
		LPCWSTR pcwstr = ws.c_str();
		hFind = FindFirstFile(pcwstr, &FindFileData);
		if (hFind == INVALID_HANDLE_VALUE)
		{
			errorCode = 2061;
			return  false;
		}
		else
		{
			char ch[260];
			char DefChar = ' ';
			if (WideCharToMultiByte(CP_ACP, 0, FindFileData.cFileName, -1, ch, 260, &DefChar, NULL) == 0)
			{
				errorCode = 2062;
				return false;
			}
			errorCode = 0;
			std::string ss(ch);
			fPath = sPath + ss;
			return true;
		}
	}
	else
	{
		errorCode = 2063;
		return false;
	}

	
}



bool COMFunc::FindQFile(std::string sFileName)
{
	HANDLE hFind;
	WIN32_FIND_DATA FindFileData;
	TCHAR szPath[MAX_PATH];
	std::string path;
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, &szPath[0])))
	{
		PathAppend(szPath, TEXT("RBSoft\\data\\"));
		std::wstring str(szPath);
		path.resize(str.size());
		std::transform(str.begin(), str.end(), path.begin(), wctob);
		PathAppend(szPath, TEXT("*.qrs"));
	}
	hFind = FindFirstFile(szPath, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		errorCode = 2001;
		HRESULT err = GetLastError();
		return false;
	}
	else
	{
		char ch[260];
		char DefChar = ' ';
		if (WideCharToMultiByte(CP_ACP, 0, FindFileData.cFileName, -1, ch, 260, &DefChar, NULL)==0)
		{
			errorCode = 2002;
			return false;
		}
		errorCode = 0;
		std::string ss(ch);/*
		if (!ConfigControl(path))
		{
			return "";
		}*/
		fPath = path + ss;
		return true;
	}
}
//-------------------


bool COMFunc::ReadFile(std::string path)
{
	std::ifstream in(path, std::ios::in | std::ios::binary);
	in.unsetf(std::ios::skipws);
	if (in.bad())
	{
		errorCode = 2011;
		return false; 
	}
	std::istream_iterator<char> inBegin(in), end;
	in.unsetf(std::ios::skipws);
	copy(inBegin, end, inserter(eFile, eFile.end()));
	if (eFile == "")
	{
		errorCode = 2012;
		return false;
	}
	return true;
}


//-------------------


bool COMFunc::Controling()
{
	if (sFile != "")
	{
		return true;
	}
	const int BUFFSIZE = 128000;
	int outlen, inlen;
	FILE *in;
	unsigned char key[32] = "rbsoftlicenseservicekeyforaes25";
	unsigned char iv[8]; 
	unsigned char inbuf[BUFFSIZE], outbuf[BUFFSIZE];
	EVP_CIPHER_CTX ctx;
	if (fopen_s(&in, fPath.c_str(), "rb")!=NULL)
	{
		errorCode = 2080;
		return false;
	}
	std::string outer;

	/* �������� �������� � �������� �������� ������������ */
	EVP_CIPHER_CTX_init(&ctx);
	EVP_DecryptInit(&ctx, EVP_aes_256_cfb(), key, iv);


	/* ��������� ������ */
	for (;;) {
		inlen = fread(inbuf, 1, BUFFSIZE, in);
		if (inlen <= 0) break;

		if (!EVP_DecryptUpdate(&ctx, outbuf, &outlen,
			inbuf, inlen)) return 0;
		if (inlen < BUFFSIZE)
		{
			break;
		}
		std::string tmp(reinterpret_cast<char*>(outbuf));
		outer = outer + tmp;
	}

	/* ��������� ������� ������������ */
	if (!EVP_DecryptFinal(&ctx, outbuf, &outlen)) return 0;
	std::string tmp(reinterpret_cast<char*>(outbuf));
	outer = outer + tmp;
	EVP_CIPHER_CTX_cleanup(&ctx);
	sFile = outer;
	for (int i = 0; i < 16; i++)
	{
		sFile[i] = ' ';
	}
	ConfigControl();
	return true;
}


//-------------------
std::string COMFunc::GetQuery(SAFEARRAY **paParams)
{
	HRESULT hr;
	LONG nIndex = 0;
	CComVariant vQueryName;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vQueryName);
	if (V_VT(&vQueryName) == VT_BSTR)
	{
		BSTR tmp = vQueryName.bstrVal;
		std::string sQuery;
		if (BstrToStdString(tmp, sQuery) == false)
		{
			errorCode = 2021;
			return ""; 
		}
		sQuery.resize(sQuery.size() - 1);
		regexpFunc rgx;
		if (rgx.GetRxQuery(sFile,sQuery, dType) != TRUE)
		{
			errorCode = rgx.errorCode;
			return "";
		}
		errorCode = 0;
		return rgx.qResult;
	}
	else
	{
		errorCode = 2022;
		return "";
	}
}
//-------------------



int COMFunc::GetParams(std::string query, SAFEARRAY **paParams)
{
	HRESULT hr;
	int i = 0;
	LONG nIndex = 1;
	CComVariant vParam;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam);
	if (V_VT(&vParam) == VT_DISPATCH)
	{
		CComPtr<IDispatch>pParams = V_DISPATCH(&vParam);
		regexpFunc regexp;
		if (!regexp.GetParams(query))
		{
			errorCode = regexp.errorCode;
			return -1;
		}
		CComVariant parametr;
		for each (std::string var in regexp.qParams)
		{
			const char * ccap = var.c_str();
			hr = pParams.Invoke1(L"��������", &variant_t(ccap), &parametr);
			if (SUCCEEDED(hr))
			{
				params[var] = parametr;
				std::vector<std::string>::iterator it;
				it = vcParamsName.end();
				vcParamsName.insert(it,var);
				i++;
			}
			else
			{
				errorCode = 2031;
				return -1;
			}
		}
		return i+1;
	}
	else
	{
		if (V_VT(&vParam) == VT_I4)
		{
			long cnt = vParam.lVal;
			return 0;
		}
		else
		{
			errorCode = 2032;
			return -1;
		}
	}
}
//-------------------



CComVariant COMFunc::Execute(std::string sQuery, CComPtr<IDispatch> m_iConnect)
{
	const char* cQuery = sQuery.c_str();
	HRESULT hr;
	CComVariant vApp;
	hr = m_iConnect.GetPropertyByName(L"AppDispatch", &vApp);
	CComPtr<IDispatch>pApp = V_DISPATCH(&vApp);
	CComVariant vQuery;
	hr = pApp.Invoke2(L"NewObject", &variant_t(L"������"), &variant_t(cQuery), &vQuery);
	if (SUCCEEDED(hr))
	{
		CComPtr<IDispatch>pQuery = V_DISPATCH(&vQuery);
		CComVariant vResQuery;
		for each (std::string var in vcParamsName)
		{
			const char* p1 = var.c_str();
			hr = pQuery.Invoke2(L"������������������", &variant_t(p1), &(params[var]), NULL);
			if (!SUCCEEDED(hr))
			{
				errorCode = 2041;
				::VariantInit(&vApp);
				vApp.boolVal = false;
				return vApp;
			}
		}
		hr = pQuery.Invoke0(L"Execute", &vResQuery);
		if (SUCCEEDED(hr))
		{
			CComPtr<IDispatch>pResQuery = V_DISPATCH(&vResQuery);
			pResQuery.Detach();
			return vResQuery;
		}
		else
		{
			errorCode = 2042;
			vApp.boolVal = false;
			::VariantInit(&vApp);
			return vApp;
		}
	}
	else
	{
		errorCode = 2043;
		vApp.boolVal = false;
		::VariantInit(&vApp);
		return vApp;
	}
}


bool COMFunc::GetUserInfo(SAFEARRAY **paParams)
{
	HRESULT hr;
	LONG nIndex = 1;
	CComVariant vParam2;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam2);
	if (V_VT(&vParam2) != VT_I4)
	{
		my_info.errorCode = 8001;
		char* c_tmp = "������: �� �������� ���������� ��������";
		memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
	long type = vParam2.lVal;
	if (type == 3)
	{
		return 0;
	}
	int ret = 0;
#pragma region get_user_info
	nIndex = 0;
	CComVariant vParam;
	hr = SafeArrayGetElement(*paParams, &nIndex, &vParam);
	if (V_VT(&vParam) == VT_DISPATCH)
	{
		CComPtr<IDispatch>pParams = V_DISPATCH(&vParam);
		CComVariant parametr;
		hr = pParams.Invoke1(L"��������", &variant_t("���"), &parametr);
		if (!SUCCEEDED(hr))
		{
			my_info.errorCode = 2120;
			char* c_tmp = "������: �� �������� ���������� ��������: ���";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
		BSTR bstr_tmp = parametr.bstrVal;
		char* p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.name[0], &p[0], sizeof(my_info.lic.reg_data.name));
		hr = pParams.Invoke1(L"��������", &variant_t("�������"), &parametr);
		if (!SUCCEEDED(hr))
		{
			my_info.errorCode = 2120;
			char* c_tmp = "������: �� �������� ���������� ��������: �������";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.fam[0], &p[0], sizeof(my_info.lic.reg_data.fam));
		hr = pParams.Invoke1(L"��������", &variant_t("��������"), &parametr);
		if (!SUCCEEDED(hr))
		{
			my_info.errorCode = 2120;
			char* c_tmp = "������: �� �������� ���������� ��������: ��������";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.otch[0], &p[0], sizeof(user.otch));
		hr = pParams.Invoke1(L"��������", &variant_t("�����������"), &parametr);
		if (!SUCCEEDED(hr))
		{
			my_info.errorCode = 2120;
			char* c_tmp = "������: �� �������� ���������� ��������: �����������";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.organ[0], &p[0], sizeof(my_info.lic.reg_data.organ));
		hr = pParams.Invoke1(L"��������", &variant_t("���"), &parametr);
		if (!SUCCEEDED(hr))
		{
			char* c_tmp = "������: �� �������� ���������� ��������: ���";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			my_info.errorCode = 2120;
			return false;
		}
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.inn[0], &p[0], sizeof(my_info.lic.reg_data.inn));
		hr = pParams.Invoke1(L"��������", &variant_t("email"), &parametr);
		if (!SUCCEEDED(hr))
		{
			char* c_tmp = "������: �� �������� ���������� ��������: ����������� �����(email)";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			my_info.errorCode = 2120;
			return false;
		}
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.email[0], &p[0], sizeof(my_info.lic.reg_data.email));
		hr = pParams.Invoke1(L"��������", &variant_t("�����"), &parametr);
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.adress[0], &p[0], sizeof(my_info.lic.reg_data.adress));
		hr = pParams.Invoke1(L"��������", &variant_t("�������"), &parametr);
		bstr_tmp = parametr.bstrVal;
		p = _com_util::ConvertBSTRToString(bstr_tmp);
		memcpy(&my_info.lic.reg_data.tel[0], &p[0], sizeof(my_info.lic.reg_data.tel));
	}
#pragma endregion ��������� ���������� � ������������
#pragma region write_user_info
	FILE *out;
	try
	{
		TCHAR szPath[MAX_PATH];
		std::string path;
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS | CSIDL_FLAG_CREATE, NULL, 0, &szPath[0])))
		{
			PathAppend(szPath, TEXT("RBSoft"));
			if (!PathIsDirectory(szPath))
			{
				CreateDirectory(szPath, 0);
			}
			PathAppend(szPath, TEXT("RBSoft1CExtension"));
			if (!PathIsDirectory(szPath))
			{
				CreateDirectory(szPath, 0);
			}
			PathAppend(szPath, TEXT("data"));
			if (!PathIsDirectory(szPath))
			{
				CreateDirectory(szPath, 0);
			}
			PathAppend(szPath, TEXT("\\ui.cnf"));
			std::wstring str(szPath);
			path.resize(str.size());
			std::transform(str.begin(), str.end(), path.begin(), wctob);
		}
		const char* pathh = path.c_str();
		if (PathFileExistsA(pathh))
		{
			remove(pathh);
		}
		char buff[1024];
		memcpy(&buff[0], &my_info.lic.reg_data, sizeof(my_info.lic.reg_data));
		if (fopen_s(&out, pathh, "wb") == NULL)
		{
			fwrite(&buff[0], sizeof(buff), 1, out);
			fclose(out);
		}
		else
		{
			HRESULT ss = GetLastError();
			my_info.errorCode = 8010;
			char* c_tmp = "������: ������ ���������� ���������������� ������������";
			memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
			return false;
		}
	}
	catch (HRESULT hh)
	{
		my_info.errorCode = 8000;
		char* c_tmp = "������: ������ ���������� ���������������� ������������";
		memcpy(&my_info.error_descrypt[0], &c_tmp[0], sizeof(c_tmp));
		return false;
	}
#pragma endregion ������ ���������� � ������������
	return true;
}



//-------------------
bool COMFunc::BstrToStdString(const BSTR bstr, std::string& dst)
{
	int cp = CP_ACP;
	if (!bstr)
	{
		dst.clear();
		return false;
	}
	int res = WideCharToMultiByte(cp, 0, bstr, -1, NULL, 0, NULL, NULL);
	if (res > 0)
	{
		dst.resize(res);
		WideCharToMultiByte(cp, 0, bstr, -1, &dst[0], res, NULL, NULL);
	}
	else
	{
		dst.clear();
		return false;
	}
	return true;
}


bool COMFunc::ConfigControl()
{
	regexpFunc rgx;
	std::string str=rgx.GetNameQueryFile(sFile);
	const char *cp = str.c_str();
	memcpy(&my_info.product[0], &cp[0], sizeof(nameQF));
	return true;
}