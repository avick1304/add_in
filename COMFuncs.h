#include "net_service.h"





class ATL_NO_VTABLE COMFunc
{ 
public:
//-----------params
	sysInfo my_info;
	long errorCode;
	CComVariant vResult;
	std::string fPath = "";
	long dType;
	char nameQF[32];
//-----------find file functions
	bool FindQFile(std::string sFileType);
	bool FindPathQFile(SAFEARRAY **paParams);
//-----------read qtext functions
	bool ReadFile(std::string path);
	bool GetQText(SAFEARRAY **paParams);
//-----------general functions
	bool MakeRequestFile();
	bool GetUserInfo(SAFEARRAY **paParams);
	bool Controling();
	std::string GetQuery(SAFEARRAY **paParams);
	int GetParams(std::string query, SAFEARRAY **paParams);
	CComVariant Execute(std::string sQuery, CComPtr<IDispatch> m_iConnect);
	bool BstrToStdString(const BSTR bstr, std::string& dst);
	bool ConfigControl();
private:
//-----------functions
	
//-----------params
	
	char  confCode[64];
	std::string sFile;
	std::string eFile;
	reg_user_list user;
	std::map<std::string, CComVariant> params;
	std::vector<std::string> vcParamsName;
};