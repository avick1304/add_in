#include "stdafx.h"
#include "packages.h"

std::vector<std::string> regexpFunc::rgFunc = {
	"#",
	"#([^#]+)",
	"([^#]+)~",
	"~([^#]+)",
	"&([�-��-�]+)",
	"@",
	"([^@]+)@",
	"([^~]+)~"
};



std::string regexpFunc::GetNameQueryFile(std::string sText)
{
	rx = rgFunc[5] + rgFunc[6];
	regex_search(sText.c_str(), cmResult, rx);
	sText = cmResult[1];
	return sText;
}


bool regexpFunc::GetParams(std::string query)
{
	rx = rgFunc[4];
	CComVariant parametr;
	int i = 0;
	while (std::regex_search(query.c_str(), cmResult, rx))
	{
		//qParams[i] = cmResult[1];
		it = qParams.end();
		std::string val = cmResult[1];
		qParams.insert(it, val);
		query = cmResult.suffix().str();
		i++;
	}
	if (i < 0)
	{
		errorCode = 3011;
		return false;
	}
	else
	{
		errorCode = 0;
		return true;
	}
}




//--------------------------------------------------------------------

bool regexpFunc::GetRxQuery(std::string sText, std::string qName, long dType)
{
	std::string sBuff;
	rx = rgFunc[0] + qName + rgFunc[1];
	regex_search(sText.c_str(), cmResult, rx);
	sBuff = cmResult[1];
	if (sBuff == "")
	{
		errorCode = 3001;
		return false;
	}
	if (dType == DEMO)
	{
		rx = rgFunc[3];
		regex_search(sBuff.c_str(), cmResult, rx);
		sBuff = cmResult[1];
	}
	else if (dType== FL)
	{
		rx = rgFunc[2];
		regex_search(sBuff.c_str(), cmResult, rx);
		sBuff = cmResult[1];
	}
	
	if (sBuff == "")
	{
		errorCode = 3002;
		return false;
	}
	rx = "~";
	qResult = regex_replace(sBuff, rx, "");
	errorCode = 0;
	return true;
}
